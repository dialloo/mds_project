
package enterprise.entity_bean_client;

import java.util.Date;
import java.util.concurrent.Future;

import javax.naming.InitialContext;

import org.apache.catalina.logger.SystemOutLogger;

import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;
import enterprise.entity_bean_api.DirectoryManager;;

/**
 * The class of the client.
 */
public final class AdministrateurClient {
	/**
	 * utility class with no instance.
	 */
	private AdministrateurClient() {
	}
	
	
	
	/**
	 * constant waiting time in ms.
	 */	
	public static final  int WAITINGTIME = 1000;

	/**
	 * the main of the client.
	 * 
	 * @param args
	 *            no command line arguments.
	 */
	public static void main(final String[] args) {
		DirectoryManager sb;
		try {
			Customer c=new Customer();
			Article a = new Article();
			InitialContext ic = new InitialContext();
			sb = (DirectoryManager) ic.lookup("enterprise.entity_bean_api.DirectoryManager");
			
			
			sb.createCustomer("dial", "Ousmane", "Dialo", "ous@gmail.com", "64 av. gaston boissier");
         	 sb.createArticle(1223, "orange","orange jaune" ,103);
         	
			
		//////	
			
			
            System.out.println(new Date().toString() + " - Begin - Synchronous disponibilité ");
            System.out.println("DirectoryManager says : " + sb.SynchroneDisponibiliteArticle(1223));
            System.out.println(new Date().toString() + " - End   - Synchronous disponibilite - Almost Instantaneous !!!");

    		System.out.println("Est ce que ce produit est disponible ?");
    		
    		Future<String> future = sb.asynchronousDisponibilite("Ce produit est maintenant disponible !");

    		while (!future.isDone()) {
    			Thread.sleep(WAITINGTIME);
    			System.out.println(new Date().toString() + " - No ce produit n'est pas disponible pour le momment ...");
    		}

    		System.out.println(new Date().toString() + " - result : " + future.get());
			 
//	////////	
//			
//			 
//			 sb.createCustomer("ousst", "Ousmane", "Dialo", "ous@gmail.com", "67 av. gaston boissier");
//			 sb.createArticle(23, "orange","orange jaune" ,13);
//			 
//			 sb.createCustomer("YOU", "teodore", "youtube", "yto@gmail.com", "55 av. henry boissier");
//			 sb.createArticle(45, "mangue","mangue rouge",1 );
//			System.out.println(sb.findCustomer("ous").getFirstName()); 
//			sb.updateCustomer("ous", "yanick", "zangue", "yan@gmail.com", "45 av etat unis");
//			System.out.println("apres modification de ous : "+sb.findCustomer("ous").getFirstName()); 
//			System.out.println();
//			
//			a=sb.findArticle(23);
//			if(a==null){System.out.println("c null");}else {
//				System.out.println(a.getCategorie()+"\n quantié :"+a.getQuantite());
//			}
//			
//			Article b=sb.findArticle(22);
//			if(b==null) {System.out.println("ce t'article est nulle");
//			}else {
//			System.out.println(a.getCategorie());
//			}
//			
//			//remove article et customer
//			sb.deleteArticle(12);
//			
//			if(sb.findArticle(12)==null) {System.out.println("ce t'article est nulle");
//			}else {
//			System.out.println(a.getCategorie());
//			}
//			
//			sb.deleteCustomer("teo");
//			
//			System.out.println(sb.findCustomer("teo").getFirstName());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
