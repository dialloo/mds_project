package enterprise.entity_bean_client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import enterprise.entity_bean_api.DirectoryManager;
import enterprise.entity_bean_api.OrderManager;
import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;
import enterprise.entity_bean_entity.Order;

public final class OrderClient {//lui il peut juste verifier la disponibilité des article et les commander
	/**
	 * utility class with no instance.
	 */
	private OrderClient() {
	}
	
	
	/**
	 * constant waiting time in ms.
	 */	
	public static final  int WAITINGTIME = 1000;
	public static void main(String[] args) {
		InitialContext ic;
		OrderManager om;
		
		
		
		try {
			ic = new InitialContext();

			DirectoryManager dm;
			om = (OrderManager) ic.lookup("enterprise.entity_bean_api.OrderManager");
			dm = (DirectoryManager) ic.lookup("enterprise.entity_bean_api.DirectoryManager");
			
			
	       
			
			Customer c=new Customer();

			Article a1= new Article();
			Article a2= new Article();
			Article a3= new Article();

			
			Collection<Article> articles=new ArrayList<Article>();

			// je cree un customer 
			dm.createCustomer("Ousmane", "Salut", "Goood", "good@gmail.com", "80 av. gean carter");
			c=dm.findCustomer("Ousmane");
	System.out.println("Nom : "+c.getPseudo()+" firstName  : "+c.getFirstName()+"  LastName :"+c.getLastName()+" email : "+c.getEmail()+" adressePostale  "+c.getPostalAddress());
			// je cree 3 articles
//			 dm.createArticle(1799, "lemonade","boisson rouge " );
//			 dm.createArticle(122, "eau","meilleur bosson " );
//			 dm.createArticle(1888, "mais","cereal du momment " );
//			 a1=dm.findArticle(1799);
//			 a2=dm.findArticle(122);
//			 a3=dm.findArticle(1888);
//			 //j'ajoute ces articles à la liste d'articles
//			 articles.add(a1);
//			 articles.add(a2);
//			 articles.add(a3);

			 
			 

			
			


	         //om.createOrder(137, "78 rue de versailles", c, articles);
	         
	         // je cree une commande 
//	         Order ord=new Order();
//	 		ord.setAddress("448 rue royale");
//	 		ord.setArticles(articles);
//	 		ord.setId(37);
//	         System.out.println(om.findOrder(137).getArticles());
//	          
//	         
//	         // je supprime ce que j'ai creer
//	         dm.deleteArticle(122);
//	         dm.deleteArticle(1799);
//	         dm.deleteArticle(1888);
//            om.deleteOrder(137);
//            dm.deleteCustomer("hello");
	
	


		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
