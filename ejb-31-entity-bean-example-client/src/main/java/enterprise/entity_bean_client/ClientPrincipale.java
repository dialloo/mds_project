package enterprise.entity_bean_client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import enterprise.entity_bean_api.DirectoryManager;
import enterprise.entity_bean_api.OrderManager;
import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;

public class ClientPrincipale {
	/**
	 * utility class with no instance.
	 */
	private ClientPrincipale() {
	}
	/**
	 * constant waiting time in ms.
	 */	
	public static final  int WAITINGTIME = 1000;

	public static void main(String[] args) {
		
		System.out.println("BIENVENUE AU MAGASIN :");
		// je veux afficher la liste des produits dispo ici
		InitialContext ic;
		OrderManager om;
		
		
		
		try {
			ic = new InitialContext();

			DirectoryManager dm;
			om = (OrderManager) ic.lookup("enterprise.entity_bean_api.OrderManager");
			dm = (DirectoryManager) ic.lookup("enterprise.entity_bean_api.DirectoryManager");
			
		//	Collection<Article> articles=new ArrayList<Article>();
			 dm.createArticle(1799, "lemonade","boisson rouge " ,11);
			 dm.createArticle(122, "eau","meilleur bosson ",8 );
			 dm.createArticle(1888, "mais","cereal du momment " ,89);

			//je teste l'affichage des articles à partir de la Bd
			testAffichageArticlesFromBD(dm);
			
			//je supprime l'article d'identifiant 1799
			dm.deleteArticle(1799);
			//et j'affice à nouveau la liste des articles disponible
			
			testAffichageArticlesFromBD(dm);
			
			
			//je test la disponibilité d'un article de façon synchrone
			 System.out.println(new Date().toString() + " - Begin - Synchronous disponibilité ");
	         System.out.println("DirectoryManager says : " + dm.SynchroneDisponibiliteArticle(122));
	         System.out.println(new Date().toString() + " - End   - Synchronous disponibilite - Almost Instantaneous !!!");
	         //je modifie juste la quantité de l'article 122 et je test à nouveau 
	         dm.updateArticle(122, "eau","meilleur bosson ",0);
	         
	         System.out.println(new Date().toString() + " - BeginSecondTest - Synchronous disponibilité ");
	         System.out.println("DirectoryManager says : " + dm.SynchroneDisponibiliteArticle(122));
	         System.out.println(new Date().toString() + " - EndSecondTest  - Synchronous disponibilite - Almost Instantaneous !!!");
	       //je test la disponibilité d'un article de façon synchrone
	         
	         TestAsynchronousDispo(dm,dm.findArticle(122).getQuantite());
	         TestAsynchronousDispo(dm,dm.findArticle(1888).getQuantite());
	         
	         //je test la creation d'un client 
			dm.createCustomer("dil", "lula", "Dialo", "lu@gmail.com", "64 av. gaston boissier");
			dm.createCustomer("jugo", "ali", "Sow", "sl@gmail.com", "43 av. gaston boissier");
			dm.createCustomer("hiero", "tolm", "Camara", "cm@gmail.com", "777 av. gaston boissier");
			//j'affiche le mail  du client  de psedo "dil"
			System.out.println("le mail \"lu@gmail.com\"  ="+dm.findCustomer("dil").getEmail()); 
			
			
			//je test l'insertion avec la methode test insert
			System.out.println("Inserting Customer and Orders... " + dm.testInsert());
			// Test query and navigation
			System.out.println("Verifying that all are inserted... " + dm.verifyInsert());
			// Get a detached instance
			Customer c = dm.findCustomer("hiero");
			// Remove entity
			System.out.println("Removing entity... " + dm.testDelete(c));
			// Query the results
			System.out.println("Verifying that all are removed... " + dm.verifyDelete("hiero"));
			

			
			


		} catch (NamingException | InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void TestAsynchronousDispo(DirectoryManager dm,int idA) throws InterruptedException, ExecutionException {
	 if (idA==0) {
		System.out.println("Est ce que ce produit est disponible ?");
			
			Future<String> future = dm.asynchronousDisponibilite("Ce produit est maintenant disponible !");

			while (!future.isDone()) {
				Thread.sleep(WAITINGTIME);
				System.out.println(new Date().toString() + " - No ce produit n'est pas disponible pour le momment ...");
			}

			System.out.println(new Date().toString() + " - result : " + future.get());
			}else {
				System.out.println("ce produit est disponible !!!");
			}
	}

	private static void testAffichageArticlesFromBD(DirectoryManager dm) {
		System.out.println("VOICI LA LISTE DES ARTICLES QUE NOUS PROPOSONS :");

		List<Article> a= dm.findAll();
		System.out.println(a.size());

		for (Article article : a) {
			System.out.println(a.toString());
			
		}
	}

}
