package enterprise.entity_bean_entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



/**
 * The entity.
 */  
@Entity
@Table(name="Article")
@NamedQuery(name="Article.findAll", query="SELECT a FROM Article a")
public class Article implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	 
	private int idArticle;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String categorie;
	/**
	 * 
	 */
	private int quantite;
	
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int q) {
		this.quantite = q;
	}
	private Order order;
	
	//private Customer Customer;
	@ManyToOne()
	@JoinColumn(name="ORDER_ID")
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	/**
	 * 
	 * @return
	 */
	@Id
	 @Column(name="IDARTICLE")
	public int getIdArticle() {
		return idArticle;
	}
	/**
	 * 
	 * @param idArticle
	 */
	public void setIdArticle(int idArticle) {
		this.idArticle = idArticle;
	}
	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 
	 * @return
	 */
	public String getCategorie() {
		return categorie;
	}
	/**
	 * 
	 * @param categorie
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	
	/**
	 * 
	 * @ManyToOne()
	@JoinColumn(name = "Customer_ID")
	 * @return
	 public Customer getCustomer() {
		return Customer;
	}*/
	
	/**
	 * 
	 * @param Customer
	 *
	 *
	 *
	public void setCustomer(Customer Customer) {
		Customer = Customer;
	} */
	
	@Override
	public String toString() {
		return "idArticle :  "+getIdArticle()+"  Categorie : "+getCategorie()+"  Description :"+getDescription()+" Qantite : "+getQuantite();
	}
	

}
