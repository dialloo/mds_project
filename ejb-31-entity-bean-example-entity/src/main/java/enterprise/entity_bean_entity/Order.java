package enterprise.entity_bean_entity;
import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The order of the customers.
 */
@Entity
@Table(name = "ORDER_TABLE")
public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * the identifier.
	 */
	private int idOrder;
	/**
	 * the shipping address.
	 */
	private String address;
	/**
	 * the corresponding customer.
	 */
	private Customer customer;
	
	/**
	 * the collection of article.
	 */
	private Collection<Article> articles = new ArrayList<Article>();
	@OneToMany(cascade = ALL, mappedBy = "order")
	public Collection<Article> getArticles() {
		return articles;
	}

	public void setArticles(Collection<Article> articles) {
		this.articles = articles;
	}

	/**
	 * gets the identifier.
	 * 
	 * @return the identifier.
	 */
	@Id
	@Column(name = "ORDER_ID")
	public int getId() {
		return idOrder;
	}

	/**
	 * sets the identifier.
	 * 
	 * @param id
	 *            the new identifier.
	 */
	public void setId(final int idOrder) {
		this.idOrder = idOrder;
	}

	/**
	 * gets the shipping address.
	 * 
	 * @return the shipping address.
	 */
	@Column(name = "SHIPPING_ADDRESS")
	public String getAddress() {
		return address;
	}

	/**
	 * sets the shipping address.
	 * 
	 * @param address
	 *            the new address.
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * gets the customer.
	 * 
	 * @return the customer.
	 */
	@ManyToOne()
	@JoinColumn(name = "IDCUSTOMER")
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * sets the customer.
	 * 
	 * @param customer
	 *            the customer.
	 */
	public void setCustomer(final Customer customer) {
		this.customer = customer;
	}
}
