package enterprise.entity_bean_entity;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;


/**
 * The entity.
 */
@Entity
public class Customer implements Serializable {

	/**
	 * the identifier of the customer.
	 */
	private String pseudo;
	/**
	 * the name of the customer.
	 */
	private String firstName;
	/**
	 * 
	 */
	private String lastName;
	/**
	 * 
	 */
	
	private String postalAddress;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	private Collection<Order> orders = new ArrayList<Order>();
	
	@OneToMany(cascade = ALL, mappedBy = "customer")
	public Collection<Order> getOrders() {
		return orders;
	}
	public void setOrders(Collection<Order> orders) {
		this.orders = orders;
	}
	
	
	/**
	 * 
	 * @return
	 */
	@Id
	@Column(name="IDCUSTOMER")
	public String getPseudo() {
		return pseudo;
	}
	/**
	 * 
	 * @param pseudo
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * 
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPostalAddress() {
		return postalAddress;
	}
	/**
	 * 
	 * @param postalAddress
	 */
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}



}
