
Pour exécuter notre application :
il faut lancer les commands suivantes
commencer par sotoper le domaine en faisant :

$ (. java8;asadmin stop-domain)

compiler et exécuter le projet en tapant ceci :
$ (. java8;mvn clean install)

Ensuite, le bean EJB peut être déployé avec l’outil Glassfish asadmin.
$ (. java8;asadmin start-domain domain1; asadmin start-database; asadmin deploy ejb-31-entity-bean-example-bean/target/entity-bean.jar)

lancer ensuit la commad suivante pour test l'application :
$ (. java8;cd ejb-31-entity-bean-example-client/; java -classpath $CLASSPATH:../ejb-31-entity-bean-example-bean/target/entity-bean.jar:../ejb-31-entity-bean-example-api/target/ejb-31-entity-bean-example-api-4.0-SNAPSHOT.jar:target/ejb-31-entity-bean-example-client-4.0-SNAPSHOT.jar enterprise/entity_bean_client/ClientPrincipale)

Enfin, Annuler le déploiement du composant et arrêter la base de donnée et stoper le domain en exécutant les commandes:
$ (. java8;asadmin undeploy entity-bean; asadmin stop-database; asadmin stop-domain domain1)
