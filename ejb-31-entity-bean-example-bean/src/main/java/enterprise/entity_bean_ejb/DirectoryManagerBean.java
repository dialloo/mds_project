package enterprise.entity_bean_ejb;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;
import enterprise.entity_bean_entity.Order;
import enterprise.entity_bean_api.DirectoryManager;

/**
 * The stateless session bean.
 */
@Stateless
public class DirectoryManagerBean implements DirectoryManager  {

	/**
	 * the reference to the entity manager, which persistence context is "pu1".
	 */
	@PersistenceContext(unitName = "pu1")
	private EntityManager em;

	@Override
	public void createCustomer(String  pseudo,String firstname, String lastname,String email,String adressepostale) {
		Customer c = new Customer();
		c.setLastName(lastname);
		 c.setFirstName(firstname);
		 c.setEmail(email);
		 c.setPseudo(pseudo);
		 c.setPostalAddress(adressepostale);
		// Persist the customer
		em.persist(c);
	}
	
	
	

	@Override
	public void updateCustomer(String  pseudo,String fristname, String lastname,String email,String adressepostale) {
       Customer c=em.find(Customer.class,pseudo );
       if (c!=null) {
    	   c.setFirstName(fristname);
    	   c.setLastName(lastname);
    	   c.setEmail(email);
    	   c.setPostalAddress(adressepostale);
    	  // c.setListArticles(o.getListArticles());
    	   
		
	} else {
		
		return;

	}
       
	}

	@Override
	public Customer findCustomer(String pseudo) {
		
       return em.find(Customer.class, pseudo);
	}

	@Override
	public void deleteCustomer(String pseudo) {
		
		 em.remove(findCustomer(pseudo));
		 
	}




	@Override
	public void createArticle(int idArticle,String categorie, String desc,int quantite) {
		Article a= new Article();
		 
		 a.setIdArticle(idArticle);
		 a.setCategorie(categorie);
		 a.setDescription(desc);
		 a.setQuantite( quantite);
		
		em.persist(a);
	}




	@Override
	public void updateArticle(int idArticle,String categorie, String desc , int quantite) {
		Article a1=em.find(Article.class, idArticle);
	       if (a1!=null) {
	    	   a1.setCategorie(categorie);
	    	   a1.setDescription(desc);
	    	   a1.setQuantite(quantite);
	    	   
			
		} else {
			
			return;

		}		
	}




	@Override
	public Article findArticle(int idArticle) {
		return em.find(Article.class, idArticle);
	}




	@Override
	public void deleteArticle(int idArticle) {
		 em.remove(findArticle(idArticle));
		
	}
	//methode qui inscrit un client de façon non asynchrone
	
	public String Inscription(Customer c) {
		return c.getFirstName()+" votre inscription a été valider avec succès Vous serez informer dès qu'il est disponible ";
	}

	
//Une methode qui me permet d'inscrire customer de façon interactive
	public void inscruptionAlert() {
		System.out.println("Donner Votre pseudo : ");
	    Scanner scan = new Scanner (System.in);
	    String pseudo = scan.nextLine();
	    if (findCustomer(pseudo)!=null) {
	    	System.out.println("Nous vous informeront dès que l'article sera disponible !");
	    }else {//j'en profite pour l'inserer dans ma clientelle
			System.out.println("Donner Votre pseudo : ");
	    	String pseud= scan.nextLine();
			System.out.println("Donner Votre firstname : ");
			String firstname = scan.nextLine();
			System.out.println("Donner Votre lastname : ");
			String lastname = scan.nextLine();
			System.out.println("Donner Votre email : ");
			String email = scan.nextLine();
			System.out.println("Donner Votre adresse postale : ");
			String adressepostale = scan.nextLine();
	    createCustomer(  pseud, firstname,  lastname, email, adressepostale);


	    }
		
	}
	
	

	//pour la notion d'asynchronous call
	
	/**
	 * the message.
	 */
	public static final  String DISPO = "Cet article est disponible !\n";

	/**
	 * the implementation of the synchronous method of the API.
	 * 
	 * @return the message.
	 */
	public String SynchroneDisponibiliteArticle(int idArt) {
		Article a= findArticle(idArt);
		if(a.getQuantite()==0)  return "L'article d'identifiant : "+a.getIdArticle()+"n'est pas disponible \n";
		return DISPO;
	}

	@Asynchronous
	public Future<String> asynchronousDisponibilite(final String name) {
	    System.out.println(new Date().toString() + " - Begin asynchronous disponibilité " + name);
	    
	    
	    //je genere un temps aléatoire 
	    Random r = new Random();
	    int Low = 10;
	    int High = 25;
	    int Result = r.nextInt(High-Low) + Low;
	    final long duration = Result * 1000;
	    try {
		Thread.sleep(duration);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    System.out.println(new Date().toString() + " - End asynchronous disponibilité " + name);
	    return new AsyncResult<String>("AsynchronousDisponibilité " + name);
	}


	
	
	
//afficher l'ensemble des articles
	@Override
	public String AfficheALLart() {
		Query q = em.createQuery("select a from Article a");
		@SuppressWarnings("rawtypes")
		List results = q.getResultList();
		if (results == null || results.size() != 0) {
			throw new RuntimeException("Unexpected number of customers after delete results : " + results.size());
		}
//		q = em.createQuery("select o from Order o");
//		results = q.getResultList();
//		if (results == null || results.size() != 0) {
//			throw new RuntimeException("Unexpected number of orders after delete");
//		}

		return "OK";
		
		
	}
	
	public List<Article> findAll() {
	    CriteriaBuilder cb = em.getCriteriaBuilder();
	    CriteriaQuery<Article> cq = cb.createQuery(Article.class);
	    Root<Article> rootEntry = cq.from(Article.class);
	    CriteriaQuery<Article> all = cq.select(rootEntry);
	    TypedQuery<Article> allQuery = em.createQuery(all);
	    return allQuery.getResultList();
	}




	@Override
	public void TestAsynchronousDispo(int idA) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public String testInsert() {
		// Create a new customer
				Customer c = new Customer();
				c.setEmail("Ulea@gmail.com");
				c.setFirstName("Kante");
				c.setLastName("Teressa");
				c.setPseudo("diallous");
				c.setPostalAddress("34 boulevard Michelet");
				
				// Persist the customer
				em.persist(c);
				// Create 2 orders
				Order order1 = new Order();
				order1.setId(34);
				order1.setAddress("1 rue rogo, Paris, France");
				Order order2 = new Order();
				order2.setId(2);
				order2.setAddress("457 sait martin, California, USA");
				// Associate orders with the customers. The association
				// must be set on both sides of the relationship: on the
				// customer side for the orders to be persisted when
				// transaction commits, and on the order side because it
				// is the owning side.
				c.getOrders().add(order1);
				order1.setCustomer(c);
				c.getOrders().add(order2);
				order2.setCustomer(c);
				return "OK";
	}
	
	
	@Override
	public String verifyInsert() {
		Customer c = findCustomer("diallous");
		Collection<Order> orders = c.getOrders();
		if (orders == null || orders.size() != 2) {
			throw new RuntimeException(
					"Unexpected number of orders: " + ((orders == null) ? "null" : "" + orders.size()));
		}
		return "OK";
	}

	@Override
	public String testDelete(final Customer c) {
		// Merge the customer to the new persistence context
		Customer c0 = em.merge(c);
		// Delete all records.
		em.remove(c0);
		return "OK";
	}
	
	@Override
	public String verifyDelete(String ps) {
		Customer c=findCustomer(ps);
		if (c!=null) {
			return " c'est pas OK";
		}
		return "OK";
	}

	
}
