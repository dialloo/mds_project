package enterprise.entity_bean_ejb;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import enterprise.entity_bean_api.OrderManager;
import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;
import enterprise.entity_bean_entity.Order;
/**
 * The stateless session bean.
 */
@Stateless
public class OrderManagerBean  implements OrderManager{
	
	/**
	 * the reference to the entity manager, which persistence context is "pu1".
	 */
	@PersistenceContext(unitName = "pu1")
	private EntityManager em;

	@Override
	public void createOrder(int idOrder, String address, Customer customer, Collection<Article> articles) {
		Order ord= new Order();
		ord.setAddress(address);
		ord.setCustomer(customer);
		ord.setId(idOrder);
		ord.setArticles(articles);
		em.persist(ord);
		
	}

	@Override
	public void updateOrder(int idOrder, String address, Customer customer, Collection<Article> articles) {
        Order ord=em.find(Order.class, idOrder)	;
        if (ord !=null) {
			ord.setId(idOrder);
			ord.setCustomer(customer);
			ord.setAddress(address);
			ord.setArticles(articles);
		} else {
              return;
		}
	}

	@Override
	public Order findOrder(int idOrder) {
		return em.find(Order.class, idOrder);
	}

	@Override
	public void deleteOrder(int idOrder) {
		em.remove(findOrder(idOrder));
		
	}
	

}
