package enterprise.entity_bean_api;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

import javax.ejb.Asynchronous;
import javax.ejb.Remote;
import javax.naming.InitialContext;

import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;
import enterprise.entity_bean_entity.Order;
import javax.ejb.Asynchronous;
import java.util.concurrent.Future;


/**
 * The API of the entity bean.
 */
@Remote
public interface OrderManager {
	
		
		public void createOrder(int idOrder,String address,Customer customer,Collection<Article> articles);
		public void updateOrder(int idOrder,String address,Customer customer,Collection<Article> articles);
		public Order findOrder(int idOrder);
		public void deleteOrder(int  idOrder); 
		
		
		
		
		
}