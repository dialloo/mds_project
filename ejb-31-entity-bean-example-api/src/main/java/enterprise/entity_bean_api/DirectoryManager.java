package enterprise.entity_bean_api;

import javax.ejb.Remote;

import enterprise.entity_bean_entity.Article;
import enterprise.entity_bean_entity.Customer;
import javax.ejb.Asynchronous;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;
/**
 * The API of the entity bean.
 */
@Remote
public interface DirectoryManager {
		public void createCustomer(String  pseudo,String fristname, String lastname,String email,String adressepostale);
		public void updateCustomer(String  pseudo,String fristname, String lastname,String email,String adressepostale);
		public Customer findCustomer(String pseudo);
		public void deleteCustomer(String pseudo); 
		
		
		public void createArticle(int idArticle,String categorie, String desc, int quantite);
		public void updateArticle(int idArticle,String categorie, String desc,int quantite);
		public Article findArticle(int idArticle);
		public void deleteArticle(int  idArticle); 
		String AfficheALLart() ;
		List<Article> findAll();
		public String testInsert();
		public String verifyInsert();
		public String testDelete(final Customer c);
		public String verifyDelete(String ps) ;
		
		/**
		 * a synchronous method of the API.
		 * 
		 * 
		 */
		String SynchroneDisponibiliteArticle(int idArticle);

		/** 
		 * an asynchronous method of the API.
		 * 
		 * 
		 */
		@Asynchronous
		Future<String> asynchronousDisponibilite(final String name);
		
		
		public void TestAsynchronousDispo(int idA); 
}
